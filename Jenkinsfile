def services = [
    [name: 'broadcaster', repo:'nchteamnch/broadcaster', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/broadcaster'],
    [name: 'crypto', repo:'nchteamnch/crypto_service', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/crypto_service'],
    [name: 'datawriter', repo:'nchteamnch/data_writer', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/data_writer'],
    [name: 'eventstream', repo:'nchteamnch/event_stream', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/eventstream'],
    [name: 'funding', repo:'nchteamnch/funding_service', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/funding_service'],
    [name: 'keystore', repo:'nchteamnch/keystore', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/keystore'],
    [name: 'sars', repo:'nchteamnch/sars', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/sars'],
    [name: 'smartcontracts', repo:'/nchteamnch/verifiable_compute_executor', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/smart_contracts'],
    [name: 'apbackend', repo: '/nchteamnch/ap_backend', image: '709855634846.dkr.ecr.eu-west-1.amazonaws.com/ap_backend'],
]

def deployments = [
    "QA": '/regtest/qa/backend-environment',
    "QA NFT": '/regtest/qanft/backend-environment',
    "staging": '/regtest/staging/backend-environment',
    "PreProd": '/regtest/preprod/backend-environment',
    "Production": '/regtest/production/backend-environment',
]

pipeline {
    agent { label "master" }
    parameters {
        string (name: 'targetEnvironment', defaultValue: 'none', description: 'String representation of the target environment.  Leave as none to update script')
        string (name: 'description', defaultValue: '', description: 'Description from the Jira ticket when updating via Jira')
    }
    triggers {
        GenericTrigger(
            causeString: 'Webhook', 
            printContributedVariables: true, 
            printPostContent: true, 
            regexpFilterExpression: '', 
            regexpFilterText: '', 
            token: 'deploytest', 
            tokenCredentialId: '',
            genericVariables: [
                [key: 'targetEnvironment', value: '$.transition.transitionName', regexpFilter: 'Deploy to ', defaultValue: 'Development'],
                [key: 'description', value: '$.issue.fields.description']
            ]
        )
    }
    stages {
        
        if (getBuildUser() != "") {
            echo 'Installed Script'
            currentBuild.result = 'SUCCESS'
            return
        }

        stage ("Test") {
            steps {
                script {
                    echo "Job Triggered"
                    echo "${targetEnvironment}"
                    echo "${description}"
                    echo "${deployments[targetEnvironment]}"
                    echo "${getBuildUser()}"
                }
            }
        }

        stage ("Get Image Versions From Jira") {
            when { allOf { expression { deployments[targetEnvironment] != null}; expression { description != ''}}}
            steps {
                script {
                    def lines = description.readLines()
                    def images = lines.inject([:]) { map, line ->
                        if (line ==~ /\|[^\*](.*?)\|/) {
                            String[] parts = line.split(/\|/)
                            def repo = services.find{it.name == parts[1]}.image
                            map[parts[1]] = repo + ':' + parts[2]
                        }
                        map
                    }

                    images.each { key, value ->
                        println key + ' : ' + value
                    }
                }
            }
        }
    }
}

@NonCPS
def getBuildUser() {
    def causes = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause')
    return causes.size() > 0 ? causes[0].userId : ""
}